// ДАННЫЕ

// кабинеты
let cabinets = [
    {
        uid: 1,
        name: 'Окно 1',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '09:00:00',
                endWork: '20:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '13:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
    {
        uid: 2,
        name: 'Окно 2',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '09:00:00',
                endWork: '20:00:00',
                lunchBreak: {
                    start: '14:00:00',
                    end: '15:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '13:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
    {
        uid: 3,
        name: 'Окно 3',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '09:00:00',
                endWork: '20:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '13:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
    {
        uid: 4,
        name: 'Окно 4',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '09:00:00',
                endWork: '20:00:00',
                lunchBreak: {
                    start: '14:00:00',
                    end: '15:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '13:00:00',
                    end: '14:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '13:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
    {
        uid: 5,
        name: 'Окно 5',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '09:00:00',
                endWork: '20:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '13:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
    {
        uid: 6,
        name: 'ТОСП 1',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '12:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
    {
        uid: 7,
        name: 'ТОСП 2',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '12:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
    {
        uid: 8,
        name: 'ТОСП 3',
        workDays: [
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: '08:00:00',
                endWork: '12:00:00',
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
            {
                startWork: null,
                endWork: null,
                lunchBreak: {
                    start: null,
                    end: null,
                },
            },
        ],
    },
];

// Сотрудники
let users = [
    {
        uid: 1,
        name: 'Сотрудник 1',
        workLists: [
            {
                day: '2020-09-01',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-04',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-03',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
        ]
    },
    {
        uid: 2,
        name: 'Сотрудник 2',
        workLists: [
            {
                day: '2020-09-01',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-04',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-03',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
        ]
    },
    {
        uid: 3,
        name: 'Сотрудник 3',
        workLists: [
            {
                day: '2020-09-07',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-08',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-10',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
        ]
    },
    {
        uid: 4,
        name: 'Сотрудник 4',
        workLists: [
            {
                day: '2020-09-25',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-28',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
            {
                day: '2020-09-29',
                cabinets: [
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '08:00:00',
                            end: '12:00:00',
                        },
                    },
                    {
                        cabinetId: 1,
                        workTime: {
                            start: '13:00:00',
                            end: '17:00:00',
                        },
                    }
                ],
                startWork: '08:00:00',
                endWork: '17:00:00',
                lunchBreak: {
                    start: '12:00:00',
                    end: '13:00:00',
                },
            },
        ]
    },
]

// График основной
let ship = {
    uid: 1,
    name: 'График на Сентябрь',
    firstDay: '2020-09-01',
    lastDay: '2020-09-30',
}



// МЕТОДЫ


/**
 * Возвращает объект сотрудника из массива users
 * или undefined
 * 
 * @param {1} id 
 */
function findUserById(id) {
    return users.find(function (iteam) {
        return iteam.uid == id;
    });
}


/**
 * Возвращает объект - рабочегий днь 
 * сотрудника из массива workLists объекта user
 * по id сотрудника и по дате
 * или undefined
 * 
 * @param {1} userUid 
 * @param {'2020-09-01'} day 
 */
function getUserOneWorkDay(userUid, day) {
    return findUserById(userUid).workLists.find(function (iteam) {
        return iteam.day == day;
    });
}


/**
 * Возвращает массив кабинетов одного сотрудника за один день
 * 
 * @param {1} userUid 
 * @param {'2020-09-01'} day 
 */
function getUserOneWorkDayCabinets(userUid, day) {
    let workDay = getUserOneWorkDay(userUid, day);
    if (workDay) {
        return workDay.cabinets.map(function (cabinet) {
            return {
                cabinet: getCabinetById(cabinet.cabinetId),
                workTime: cabinet.workTime,
            }
        });
    }
}


/**
 * Возвращает объект все кабинеты сотрудника за конкретный день
 * {
 *  cabinetName: "Орехово Окно 1 ", - склееное название кабинетов за один день
 *  errors: 0, - расхождение от нормативного времени работы сотрудника (- переработка, + недоработка)
 *  sumWorkTime: 8, - сумма рабочего времени в кабинетах
 * }
 * 
 * @param {1} userUid 
 * @param {'2020-09-01'} day 
 */
function makeSumWorkTimeOneDay(userUid, day) {
    let workDay = getUserOneWorkDay(userUid, day);
    let workDayCabinets = getUserOneWorkDayCabinets(userUid, day);
    if (workDayCabinets) {
        let cabinetName = '';
        let sumWorkTime = 0;
        let normalWorkTime = getWorkHours(workDay.startWork, workDay.endWork) - getWorkHours(workDay.lunchBreak.start, workDay.lunchBreak.end);
        let prevCabinetName = '';
        for (let cabinet of workDayCabinets) {
            if (prevCabinetName != cabinet.cabinet.name) {
                cabinetName += `${cabinet.cabinet.name} `;
            }
            prevCabinetName = cabinet.cabinet.name
            sumWorkTime += getWorkHours(cabinet.workTime.start, cabinet.workTime.end);
        }
        return {
            cabinetName,
            sumWorkTime,
            errors: normalWorkTime - sumWorkTime,
        }
    }
}


/**
 * Количество часов между двумя временными метками
 * 
 * @param {'08:00:00'} timeStart 
 * @param {'17:00:00'} timeEnd 
 */
function getWorkHours(timeStart, timeEnd) {
    if (timeStart && timeEnd) {
        let startParam = timeStart.split(':');
        let start = new Date();
        start.setHours(startParam[0], startParam[1], startParam[2]);
        let endParam = timeEnd.split(':');
        let end = new Date();
        end.setHours(endParam[0], endParam[1], endParam[2]);
        return Math.round((end - start) / 1000 / 3600 * 10) / 10;
    }
    return 0;
}


/**
 * День недели в зависимости от номера
 * 0 - вс, 1 - пн, ..., 6 - сб
 * @param {0-6} deyNumber 
 */
function getWeekday(deyNumber) {
    let weekName = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
    return weekName[deyNumber];
}


// Список всех дней в заданом графике работы
/**
    [
    0: {curDate: "2020-09-01", day: "01", mon: "09", year: 2020, type: 2},
    1: {curDate: "2020-09-02", day: "02", mon: "09", year: 2020, type: 3},
    ...
    29: {curDate: "2020-09-30", day: 30, mon: "09", year: 2020, type: 3}
    length: 30
    __proto__: Array(0)
    ]
 */
function getDays() {
    let firstDay = new Date(ship.firstDay);
    let lastDay = new Date(ship.lastDay);
    let days = [];
    //let countDays = (lastDay - firstDay) / 86400000;
    for (let day = firstDay; day <= lastDay; day.setDate(day.getDate() + 1)) {
        days.push({
            curDate: `${day.getFullYear()}-${(day.getMonth() + 1) < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1)}-${day.getDate() < 10 ? '0' + day.getDate() : day.getDate()}`,
            day: day.getDate() < 10 ? '0' + day.getDate() : day.getDate(),
            mon: (day.getMonth() + 1) < 10 ? '0' + (day.getMonth() + 1) : (day.getMonth() + 1),
            year: day.getFullYear(),
            type: day.getDay(),
        });
    }
    return days;
}


/**
 * Возвращает массив - все рабочие часы из user.woerLists 
 * или 0 если запись отсутствует
 * по всему массиву users
 * по всем датам из ship
 */
function getWorkerList() {
    let dayList = getDays();
    let result = users.map(function (user) {
        return {
            name: `uid: ${user.uid} - ${user.name}`,
            workerLists: user.workLists,
            workTime: dayList.map(function (dates) {
                let userOneWorkDay = getUserOneWorkDay(user.uid, `${dates.curDate}`);
                if (userOneWorkDay) return getWorkHours(userOneWorkDay.startWork, userOneWorkDay.endWork) - getWorkHours(userOneWorkDay.lunchBreak.start, userOneWorkDay.lunchBreak.end);
                return 0;
            }),
        };
    });
    return result;
}


/**
 * Возвращает оъект кабинета по uid
 * @param {1} id 
 */
function getCabinetById(id) {
    return cabinets.find(function (cabObj) {
        return cabObj.uid == id;
    });
}


/**
 * Общее количество рабочих часов кабинета
 * 
 * @param {1} uid 
 */
function getSumCabinetWorkHours(uid) {
    let cabinet = getCabinetById(uid);
    let dayList = getDays();
    return dayList.map(function (day) {
        let cabinetWorkDay = cabinet.workDays[day.type];
        return getWorkHours(cabinetWorkDay.startWork, cabinetWorkDay.endWork) - getWorkHours(cabinetWorkDay.lunchBreak.start, cabinetWorkDay.lunchBreak.end);
    }).reduce(function (prev, val) {
        return prev + val;
    }, 0);
}


function allCabinetWorkDays(id) {
    let days = getDays();
    let cabinet = getCabinetById(id);    
    let cabinetWorkDays =  [];
    for (let day of days) {
        if (cabinet.workDays[day.type].startWork) {
            cabinetWorkDays.push({
                type: day.type,
                curDate: day.curDate,
                cabinetWorkList: cabinet.workDays[day.type],
            });
        }
    }

    return cabinetWorkDays;
}
console.log(allCabinetWorkDays(7));

function calcWorkTime(objWorkList) {
    let startW = objWorkList.startWork ?? 0;
    let enfW = objWorkList.endWork ?? 0;
    let startBL = objWorkList.lunchBreak.start;
    let endLB = objWorkList.lunchBreak.end;
    return getWorkHours(startW, enfW) - getWorkHours(startBL, endLB);
}

function checkFillCabinet(id, dayType) {
    let allWorkDays = allCabinetWorkDays(id);
    let result =  [];
    for (let cabinetWorkDay of allWorkDays) {
        if (cabinetWorkDay.type == dayType) {
            // TODO
            let fact = 8;
            let normTime = calcWorkTime(cabinetWorkDay.cabinetWorkList);
            let smile = '';
            if (normTime - fact == 0) {
                smile = '&#128515';
            } else {
                smile = '&#128544';
            }
            result.push({
                normTime,
                fact,
                smile,
            });
        }
    }
    return result;
}
console.log(checkFillCabinet(7, 1));


/**
 * Перенос графика работы кабинета в ворк-лист сотрудника
 * 
 * @param {1} cabinetId 
 * @param {1} userId 
 */
function addCabinetWorkTimeInUserWorkList(cabinetId, userId) {
    let cabinet = getCabinetById(cabinetId);
    let user = findUserById(userId);
    let dayList = getDays();
    let result = [];
    for (let day of dayList) {
        let userWorkList = getUserOneWorkDay(userId, day.curDate);
        if (cabinet.workDays[day.type].startWork) {
            let newWorkList = {
                day: day.curDate,
                cabinets: [
                    {
                        cabinetId: cabinetId,
                        workTime: {
                            start: cabinet.workDays[day.type].startWork,
                            end: cabinet.workDays[day.type].endWork,
                        },
                    },
                ],
                startWork: cabinet.workDays[day.type].startWork,
                endWork: cabinet.workDays[day.type].endWork,
                lunchBreak: {
                    start: cabinet.workDays[day.type].lunchBreak.start,
                    end: cabinet.workDays[day.type].lunchBreak.end,
                },
            };
            if (userWorkList) {
                /*let index = user.workLists.indexOf(day.curDate, 0);
                userWorkList.splice(index, 1, newWorkList);*/
            } else {
                user.workLists.push(newWorkList);
                
            }
        }
    }
    return result;
}


/**
 * Перенос графика одного дня кабинета в ворк-лист сотрудника
 * 
 * @param {1} cabinetId 
 * @param {1} dayType 
 * @param {1} userId 
 */
function addCabinetOneDayWorkTimeInUserWorkList(cabinetId, dayType, userId) {
    let cabinet = getCabinetById(cabinetId);
    let user = findUserById(userId);
    let dayList = getDays();
    let result = [];
    for (let day of dayList) {
        let userWorkList = getUserOneWorkDay(userId, day.curDate);
        if (day.type == dayType && cabinet.workDays[day.type].startWork) {
            let newWorkList = {
                day: day.curDate,
                cabinets: [
                    {
                        cabinetId: cabinetId,
                        workTime: {
                            start: cabinet.workDays[day.type].startWork,
                            end: cabinet.workDays[day.type].endWork,
                        },
                    },
                ],
                startWork: cabinet.workDays[day.type].startWork,
                endWork: cabinet.workDays[day.type].endWork,
                lunchBreak: {
                    start: cabinet.workDays[day.type].lunchBreak.start,
                    end: cabinet.workDays[day.type].lunchBreak.end,
                },
            };
            if (userWorkList) {
                /*Перезапись ворклиста*/
            } else {
                user.workLists.push(newWorkList);                
            }
        }
    }
    return result;
}


// РЕНДЕРИНГ 

// Рисуем кабинеты
function showCabinets() {
    let htmlCabinets = $('#cabinets');
    htmlCabinets.empty();
    htmlCabinets.append(`
        <h1>Общее рабочее время кабинетов: ${
        cabinets.reduce(function (prev, cabinet) {
            return prev + getSumCabinetWorkHours(cabinet.uid);
        }, 0)
        }</h1>
        ${cabinets.map(function (cabinet) {
            return `
                <div class="cabinet" data-uid="${cabinet.uid}">
                    <h2>uid: ${cabinet.uid} - ${cabinet.name}</h2>
                    <h3>Сумма рабочих часов: ${getSumCabinetWorkHours(cabinet.uid)}</h3>
                    <table>
                        <thead>
                            <tr>
                                <th>ДН</th>
                                <th>Рабочее время</th>
                                <th>Перерыв</th>
                                <th>Загрузка</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${cabinet.workDays.map(function (workDay, index) {
                if (workDay.startWork != null)
                    return `
                        <tr >                
                            <td><div class="day-in-cabinet" data-cabinet_id="${cabinet.uid}" data-day_type="${index}">${getWeekday(index)}</div></td>
                            <td>${workDay.startWork.slice(0, 5)} - ${workDay.endWork.slice(0, 5)}</td>
                            <td>${workDay.lunchBreak.start ? workDay.lunchBreak.start.slice(0, 5) : ''} ${workDay.lunchBreak.end ? workDay.lunchBreak.end.slice(0, 5) : ''}</td>
                            <td>${
                                checkFillCabinet(cabinet.uid, index).map(function(val) {
                                    return `<a title="${val.normTime} / ${val.fact}">${val.smile}</a>`;
                                }).join('')
                            }</td>
                        </tr>
                    `;
            }).join('')}
                        </tbody>
                    </table>
                </div>`
        }).join('')}
    `);
    console.log('showCabinets - done');
}

// Рисуем график
function showShip() {
    let html_ship = $('#ship');
    html_ship.empty();
    html_ship.append(`
        <h1>uid:${ship.uid} - ${ship.name}</h1>
        <table id="table_ship">
            <thead>
                <tr>
                    <th>ФИО</th>
                    ${getDays().map((dayObj) => `<th>${dayObj.day}.${dayObj.mon}<br />${getWeekday(dayObj.type)}</th>`).join('')}
                </tr>
            </thead>
            <tbody>
                ${getWorkerList().map(function (iteam) {
        return `<tr>
                        <td>${iteam.name}</td>
                        ${iteam.workTime.map(function (value) {
            return `<td>${value}</td>`
        }).join('')}
                    </tr>`;
    }).join('')}
            </tbody>
        </table>
    `);
    console.log('showShip - done');
}

// Рисуем Сотрудников
function showUsers() {
    let html_users = $('#users');
    html_users.empty();
    html_users.append(`
        <h1>Общее рабочее время сотрудников: ${
        users.reduce(function (prevUsers, user) {
            return prevUsers + user.workLists.reduce(function (prev, workList) {
                return prev + (getWorkHours(workList.startWork, workList.endWork) - getWorkHours(workList.lunchBreak.start, workList.lunchBreak.end));
            }, 0);
        }, 0)
        }</h1>
        ${users.map(function (user) {
            return `
            <div class="user-list">
                <div class="user" data-uid="${user.uid}"><h2>uid : ${user.uid} - ${user.name}</h2></div>
                <h3>Сумма часов: ${
                user.workLists.reduce(function (prev, workList) {
                    return prev + (getWorkHours(workList.startWork, workList.endWork) - getWorkHours(workList.lunchBreak.start, workList.lunchBreak.end));
                }, 0)
                }</h3>
                <button class="clear-worklist" data-user_id="${user.uid}">Очистить ворк-лист</button>
                <div class="user-table">
                    <table class="user-worklist" data-uid="${user.uid}">
                        <thead>
                            <tr>
                                <th>День</th>
                                <th>Присутствие</th>
                                <th>Перерыв</th>
                                <th>Кабинет</th>
                            </tr>
                        </thead>
                        <tbody>${
                    getDays().map((dayObj) => `
                            <tr>
                                <td>${dayObj.day} ${getWeekday(dayObj.type)}</td>
                                <td>${
                        getUserOneWorkDay(user.uid, `${dayObj.year}-${dayObj.mon}-${dayObj.day}`) ? `${getUserOneWorkDay(user.uid, `${dayObj.year}-${dayObj.mon}-${dayObj.day}`).startWork.slice(0, 5)} - ${getUserOneWorkDay(user.uid, `${dayObj.year}-${dayObj.mon}-${dayObj.day}`).endWork.slice(0, 5)}` : ''
                        }</td>
                                <td>${
                                    getUserOneWorkDay(user.uid, dayObj.curDate) ? (
                                        getUserOneWorkDay(user.uid, dayObj.curDate).lunchBreak.start ? `${getUserOneWorkDay(user.uid, dayObj.curDate).lunchBreak.start.slice(0, 5)} - ${getUserOneWorkDay(user.uid, dayObj.curDate).lunchBreak.end.slice(0, 5)}` : ''
                                        ) : ''
                        }</td>
                                <td>${
                        makeSumWorkTimeOneDay(user.uid, `${dayObj.year}-${dayObj.mon}-${dayObj.day}`) ? makeSumWorkTimeOneDay(user.uid, `${dayObj.year}-${dayObj.mon}-${dayObj.day}`).cabinetName : ''
                        }</td>
                            </tr>`).join('')
                    }
                        </tbody>
                    </table>
                </div>
            </div>
            `;
        }).join('')}
    `);
    console.log('showUsers - done');
}


//console.log(getWorkerList());

//ship.name = "Lheujq";
//setTimeout(showShip, 2000);


// Обработка действий пользователя

// Рендеринг
function startDrop() {
    showCabinets()
    showShip();
    showUsers();
    // Перемещение сотрудника в график
    $('.user').draggable();
    $('.cabinet').droppable({
        drop: function(cabinet, user) {
            console.log($(this).attr('data-uid'));
            console.log(user.draggable.attr('data-uid'));

            console.log(addCabinetWorkTimeInUserWorkList($(this).attr('data-uid'), user.draggable.attr('data-uid')));
            
            //showUsers();
            startDrop();
            //location.reload();
        },
        activate: function() {
            //console.log($(this));
            $(this).css({"background-color": "LawnGreen",});
        },
        deactivate: function() {
            //console.log($(this));
            $(this).css({"background-color": "White",});
        },
        over: function() {
            //console.log($(this));
            $(this).css({"background-color": "Green",});
        },
        out: function() {
            //console.log($(this));
            $(this).css({"background-color": "LawnGreen",});
        },
        accept: '.user',
    });
    // Перемещение дня графика кабинета в сотрудника
    $('.day-in-cabinet').draggable();
    $('.user-worklist').droppable({
        drop: function (userWorklist, dayInCabinet) {
            console.log($(this).attr('data-uid'));
            console.log(dayInCabinet.draggable.attr('data-cabinet_id'));
            console.log(dayInCabinet.draggable.attr('data-day_type'));
            addCabinetOneDayWorkTimeInUserWorkList(dayInCabinet.draggable.attr('data-cabinet_id'), dayInCabinet.draggable.attr('data-day_type'), $(this).attr('data-uid'));
       
            startDrop();
        },
        activate: function() {
            //console.log($(this));
            $(this).css({"background-color": "LawnGreen",});
        },
        deactivate: function() {
            //console.log($(this));
            $(this).css({"background-color": "White",});
        },
        over: function() {
            //console.log($(this));
            $(this).css({"background-color": "Green",});
        },
        out: function() {
            //console.log($(this));
            $(this).css({"background-color": "LawnGreen",});
        },
        accept: '.day-in-cabinet',
    });
}

// Обнуление ворк-листа
$('#users').on('click', '.clear-worklist', function() {
    let user_id = $(this).attr('data-user_id');
    let user = findUserById(user_id);
    user.workLists = [];
    startDrop();
});

$(function(){
    startDrop();
});