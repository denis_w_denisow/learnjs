describe("pow", function () {

    it("для отрицательных n возвращает NaN", function () {
        assert.isNaN(pow(2, -1));
    });

    it("для дробных n возвращает NaN", function () {
        assert.isNaN(pow(2, 1.5));
    });

    it("2 в степени 3 будет 8", function () {
        assert.equal(pow(2, 3), 8);
    });

    it("3 в степени 4 будет 81", function () {
        assert.equal(pow(3, 4), 81);
    });


});