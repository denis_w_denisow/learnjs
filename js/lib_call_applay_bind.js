// CALL, APPLAY, BIND
// call
let worker = {
    someMethod() {
        return 1;
    },

    slow(x) {
        alert("Called with " + x);
        return x * this.someMethod(); // (*)
    }
};

function cachingDecorator(func) {
    let cache = new Map();
    return function (x) {
        if (cache.has(x)) {
            return cache.get(x);
        }
        let result = func.call(this, x); // теперь 'this' передаётся правильно
        cache.set(x, result);
        return result;
    };
}

worker.slow = cachingDecorator(worker.slow); // теперь сделаем её кеширующей

alert(worker.slow(2)); // работает
alert(worker.slow(2)); // работает, не вызывая первоначальную функцию (кешируется)


// applay
let worker = {
    slow(min, max) {
        alert(`Called with ${min},${max}`);
        return min + max;
    }
};

function cachingDecorator(func, hash) {
    let cache = new Map();
    return function () {
        let key = hash(arguments); // (*)
        if (cache.has(key)) {
            return cache.get(key);
        }

        let result = func.apply(this, arguments)
        //let result = func.call(this, ...arguments); // (**)

        cache.set(key, result);
        return result;
    };
}

function hash(args) {
    return args[0] + ',' + args[1];
}

worker.slow = cachingDecorator(worker.slow, hash);

alert(worker.slow(3, 5)); // работает
alert("Again " + worker.slow(3, 5)); // аналогично (из кеша)


// Эти два вызова почти эквивалентны:

func.call(context, ...args); // передаёт массив как список с оператором расширения
func.apply(context, args);   // тот же эффект

// Есть только одна небольшая разница:

// - Оператор расширения ...позволяет передавать перебираемый объект args в виде списка в call.
// А apply принимает только псевдомассив args.

// Передача всех аргументов вместе с контекстом другой функции называется «перенаправлением вызова» (call forwarding).
// Простейший вид такого перенаправления:

let wrapper = function () {
    return func.apply(this, arguments);
};


// bind

let bound = func.bind(context, [arg1], [arg2], [argN]);

// Метод bind - позволяет зафиксировать this
let user = {
    firstName: "Вася"
};

function func() {
    alert(this.firstName);
}

let funcUser = func.bind(user);
funcUser(); // Вася

// Если у объекта много методов и мы планируем их активно передавать, 
// то можно привязать контекст для них всех в цикле:

for (let key in user) {
    if (typeof user[key] == 'function') {
        user[key] = user[key].bind(user);
    }
}

// Некоторые JS-библиотеки предоставляют встроенные функции для удобной массовой привязки контекста, например _.bindAll(obj) в lodash.

function mul(a, b) {
    return a * b;
}

let triple = mul.bind(null, 3);

alert(triple(3)); // = mul(3, 3) = 9
alert(triple(4)); // = mul(3, 4) = 12
alert(triple(5)); // = mul(3, 5) = 15