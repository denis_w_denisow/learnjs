// МАССИВЫ
let arr = [];
arr.push(...items); // добавляет элементы в конец,
arr.pop(); // извлекает элемент из конца,
arr.shift(); // извлекает элемент из начала,
arr.unshift(...items); // добавляет элементы в начало.
arr.splice(index, deleteCount, elem1, elemN) // Удаляет и вставляет элементы, отрицательный индекс - отсчет с конца (deleteCount, elem1, elemN - не обязательные параметры)
arr.slice([start], [end]); // копирует элементы массива от start до end (не включая end)
arr.concat(arg1, arg2); // создаёт новый массив, в который копирует данные из других массивов и дополнительные значения

// Перебор массива с действием над каждым элементом
arr.forEach(function (item, index, array) {
    // ... делать что-то с item
});
for (let item of arr) {
    // item - значение элемента массива
}

// Поиск в массивве 
arr.indexOf(item, from); // ищет item, начиная с индекса from, и возвращает индекс, на котором был найден искомый элемент, в противном случае -1.
arr.lastIndexOf(item, from); // – то же самое, но ищет справа налево.
arr.includes(item, from); // – ищет item, начиная с индекса from, и возвращает true, если поиск успешен
let result = arr.find(function (item, index, array) {
    // если true - возвращается текущий элемент и перебор прерывается
    // если все итерации оказались ложными, возвращается undefined
});
let results = arr.filter(function (item, index, array) {
    // если true - элемент добавляется к результату, и перебор продолжается
    // возвращается пустой массив в случае, если ничего не найдено
});

// Преобразование map, reverse, split, join, reduce
let result = arr.map(function (item, index, array) {
    // возвращается новое значение вместо элемента
});
let arr = [1, 2, 3, 4, 5];
arr.reverse();
alert( arr ); // 5,4,3,2,1
let names = 'Вася, Петя, Маша';
let arr = names.split(', '); // У метода split есть необязательный второй числовой аргумент – ограничение на количество элементов в массиве. Если их больше, чем указано, то остаток массива будет отброшен
for (let name of arr) {
  alert( `Сообщение получат: ${name}.` ); // Сообщение получат: Вася (и другие имена)
}
let arr = ['Вася', 'Петя', 'Маша'];
let str = arr.join(';'); // объединить массив в строку через ;
alert( str ); // Вася;Петя;Маша
let value = arr.reduce(function(previousValue, item, index, array) {
    // используются для вычисления какого-нибудь единого значения на основе всего массива
    // initial - начальное значение для previousValue, previousValue - результат итерации
  }, [initial]);

// Сортировка sort
function compareNumeric(a, b) {
    if (a > b) return 1;
    if (a == b) return 0;
    if (a < b) return -1;
}
let arr = [1, 2, 15];
arr.sort(compareNumeric);
alert(arr);  // 1, 2, 15
let arr = [ 1, 2, 15 ];
arr.sort(function(a, b) { return a - b; });
alert(arr);  // 1, 2, 15
arr.sort( (a, b) => a - b );

// Проверка на массив isArray
alert(Array.isArray({})); // false
alert(Array.isArray([])); // true

// Поиск find, filter, map
arr.find(func, thisArg);
arr.filter(func, thisArg);
arr.map(func, thisArg);
// ...
// thisArg - это необязательный последний аргумент
let army = {
    minAge: 18,
    maxAge: 27,
    canJoin(user) {
        return user.age >= this.minAge && user.age < this.maxAge;
    }
};
let users = [
    { age: 16 },
    { age: 20 },
    { age: 23 },
    { age: 30 }
];
// найти пользователей, для которых army.canJoin возвращает true
let soldiers = users.filter(army.canJoin, army);
alert(soldiers.length); // 2
alert(soldiers[0].age); // 20
alert(soldiers[1].age); // 23

// Преобразование в массив
str = "Итерируемый объект, Строка или Псевдомассив";
arr = Array.from(str); // Преобразует псевдомассив в массив