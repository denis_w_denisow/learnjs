// «Символ» представляет собой уникальный идентификатор.
// Создаются новые символы с помощью функции Symbol():

// Создаём новый символ - id
let id = Symbol();

// Создаём символ id с описанием (именем) "id"
let id = Symbol("id");

let id1 = Symbol("id");
let id2 = Symbol("id");

alert(id1 == id2); // false

// К примеру, alert ниже выдаст ошибку:

let id = Symbol("id");
alert(id); // TypeError: Cannot convert a Symbol value to a string

// Это – языковая «защита» от путаницы, ведь строки и символы – принципиально разные типы данных и не должны неконтролируемо преобразовываться друг в друга.
// Если же мы действительно хотим вывести символ с помощью alert, то необходимо явно преобразовать его с помощью метода .toString(), вот так:

let id = Symbol("id");
alert(id.toString()); // Symbol(id), теперь работает

// Или мы можем обратиться к свойству symbol.description, чтобы вывести только описание:

let id = Symbol("id");
alert(id.description); // id

let user = {
    name: "Вася"
};

let id = Symbol("id");

user[id] = 1;

alert(user[id]); // мы можем получить доступ к данным по ключу-символу

// Свойства, чьи ключи – символы, не перебираются циклом for..in.

let id = Symbol("id");
let user = {
  name: "Вася",
  age: 30,
  [id]: 123
};

for (let key in user) alert(key); // name, age (свойства с ключом-символом нет среди перечисленных)

// хотя прямой доступ по символу работает
alert( "Напрямую: " + user[id] );

