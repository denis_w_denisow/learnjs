"use strict"

//alert("Я JavaScript!");

//alert(prompt("Веше имя?"));

/*let userName = prompt("Кто там?");
if(userName == "Админ") {
    let pasword = prompt("Пароль?");
    if(pasword === "Я главный") {
        alert("Здравствуйте!");
    } else {
        if (pasword) {
            alert("Неверный пароль!");
        } else {
            alert("Отменено!")
        }
    }
} else {
    if (userName) {
        alert("Я вас не знаю!");
    } else {
        alert("Отменено!")
    }
}*/

/*for(let i=2; i <= 10; i+=2) {
    alert(i);
}*/

/*for(let i=1; i <= 10; i++) {
    if (i%2) continue;
    alert(i);
}*/

/*let i=0;
while(i < 3) {
    alert( `number ${i}!` );
    i++;
}*/

/*let num = 0;
while (true) {
    num = prompt("Введите число больше 100")
    if (num > 100 || num === null) break;
}*/

/*let a = 2 + 2;
switch (a) {
  case 3:
    alert( 'Маловато' );
  case 4:
    alert( 'В точку!' );
  case 5:
    alert( 'Перебор' );
  default:
    alert( "Нет таких значений" );
}*/

function pow(x, n) {
    if (n < 0) return NaN;
    if (Math.round(n) != n) return NaN;
    let result = 1;
    for (let i = 0; i < n; i++) {
        result *= x;
    }
    return result;
}

function isEmptyObject(object) {
    for (let key in object) {
        return false;
    }
    return true;
}

/*let user = {};
alert(isEmptyObject(user));
user.age = 30;
alert(isEmptyObject(user));
delete user.age;
alert(isEmptyObject(user));
let id = Symbol('idname');
user[id] = 1234;
alert(isEmptyObject(user));
alert(user[id]);*/

/*let user = {
    name: "Denis",
    say() {
        let myNameIs = () => {
            alert(this.name);
        };
        myNameIs();
    },
};
(user.say)();*/

/*let calc = {
    one: 0,
    two: 0,
    sum() {
        return +this.one + +this.two;
    },
    umn() {
        return +this.one * +this.two;
    },
};
calc.one = prompt("Первый число");
calc.two = prompt("Второй число");
alert(calc.sum());
alert(calc.umn());*/

/*function sum(obj) {
    let result = 0;
    for (let key in obj) {
        result += obj[key];
    }
    return result;
}
let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
}
alert(sum(salaries));*/

/*function multiplyNumeric() {
    for (let key in menu) {
        if(typeof(menu[key]) == 'number') menu[key] *= 2;
    }
}
// до вызова функции
let menu = {
    width: 200,
    height: 300,
    title: "My menu"
  };
  multiplyNumeric(menu)
  console.log(menu);*/

/*function Calculator() {
    this.read = function () {
        this.a = +prompt("First number:");
        this.b = +prompt("Two number:");
    };
    this.sum = function () {
        return this.a + this.b;
    };
    this.mul = function () {
        return this.a * this.b;
    };
}
let calc = new Calculator();
calc.read();
alert(calc.mul());*/

/*function Akkum(startValue) {
    this.value = startValue;
    this.read = function() {
        this.value += +prompt("new val for sum:");
    }
}
let akk = new Akkum(0);
akk.read();
akk.read();
alert(akk.value);*/

/*function random(min, max) {
    return min + Math.random() * (max - min);
}
alert(random(1, 5));
alert(random(1, 5));
alert(random(1, 5));*/

//alert( String.fromCodePoint(100) );

function showArr(arr) {
    for (let mus of arr) {
        console.log(mus);
    }
}
/*let arr = ['Doors', 'Splin', 'Ariya'];
arr.push('DM');
showArr(arr);
console.log("delete " + arr.pop());
console.log("delete " + arr.shift());
console.log(arr.unshift('Depeshe Mode'));
showArr(arr);*/

/*let styles = ['Джаз', 'Блюз'];
styles.push('Рок-н-рол');
let centrIndex = Math.trunc(styles.length / 2);
styles[centrIndex] = 'Классика';
alert(styles.shift());
styles.unshift('Рэп', 'Регги');
showArr(styles);*/

/*function camelize(str) {
    let arr = str.split('-');
    return arr.map(function(iteam){
        return [iteam[0].toUpperCase()].concat(iteam.slice(1)).join('');
    }).join('');
}
let str = 'list-style-image';
alert(camelize(str));*/

/*function filterRange(arr, a, b) {
    return arr.filter(
        (item) => {
            return (a <= item && item <= b)
        }
    );
}
let arr = [5, 3, 8, 1];
let filtered = filterRange(arr, 1, 4);
alert(filtered); // 3,1 (совпадающие значения)
alert(arr); // 5,3,8,1 (без изменений)*/

/*let arr = [5, 3, 3, 8, 1, 9, 4];
function filterRangeInPlace(arr, a, b) {
    for (let i = 0; i < arr.length; i++) {
        //console.log(i + ' - ' + arr[i]);
        if (arr[i] < a || arr[i] > b) {
            arr.splice(i, 1);
            i--;
        }
        //console.log(i + ' - ' + arr[i]);
    }
}
filterRangeInPlace(arr, 1, 4); // удалены числа вне диапазона 1..4
console.log(arr); // [3, 1];*/

/*let arr = [5, 2, 1, -10, 8];
arr.sort((a, b) => b - a);
alert(arr);*/

/*function copySorted(arr) {
    return arr.slice().sort();
}
let arr = ["HTML", "JavaScript", "CSS"];
let sorted = copySorted(arr);
alert( sorted ); // CSS, HTML, JavaScript
alert( arr ); // HTML, JavaScript, CSS (без изменений)*/

function Calculator() {
    this.methods = {
        "+": (a, b) => a + b,
        "-": (a, b) => a - b,
    };
    this.calculate = function (str) {
        let arr = str.split(' ');
        let a = +arr[0];
        let op = arr[1];
        let b = +arr[2];

        if (!this.methods[op] || isNaN(a) || isNaN(b)) return NaN;

        return this.methods[op](a, b);
    };
    this.addMethod = (name, func) => this.methods[name] = func;
}

/*let calc = new Calculator;
for (let met in calc) {
    console.log(met);
}
console.log(calc.calculate("5 - 15"));
calc.addMethod("*", (a, b) => a * b);
console.log(calc);
console.log(calc.calculate("5 * 5"));
calc.addMethod("/", (a, b) => a / b);
console.log(calc.calculate("15 / 5"));*/

/*let vasya = {name: "Вася", age: 25};
let petya = {name: "Петя", age: 30};
let masha = {name: "Маша", age: 28};
let users = [vasya, petya, masha];
let names = users.map((user) => user.name).join(', ');
alert(names);*/

/*let vasya = {name: "Вася", surname: "Пупкин", age: 25, id: 1,};
let petya = {name: "Петя", surname: "Иванов", age: 30, id: 2,};
let masha = {name: "Маша", surname: "Петрова", age: 28, id: 3,};
let users = [vasya, petya, masha];
let usersMapped = users.map((user) => {
    return {
        fullName: `${user.name} ${user.surname}`,
        id: user.id
    };
});
console.log(usersMapped);*/

/*let vasya = {name: "Вася", age: 25};
let petya = {name: "Петя", age: 30};
let masha = {name: "Маша", age: 28};
let users = [vasya, petya, masha];
function sortByAge(users) {
    users.sort((a, b) => a.age > b.age ? 1 : -1);
}
sortByAge(users);
console.log(users);*/

/*let vasya = {name: "Вася", age: 25};
let petya = {name: "Петя", age: 30};
let masha = {name: "Маша", age: 28};
let users = [vasya, petya, masha];
function getAverageAge(users) {
    return Math.round(users.reduce((prev, item) => prev += item.age, 0) / users.length);
}
console.log(getAverageAge(users));*/

/*function unique(arr) {
    let new_arr = [];
    for(let item of arr) {
        if (!new_arr.includes(item)) new_arr.push(item); 
    }
    return new_arr;
}
let str = ["кришна", "кришна", "харе", "харе", "харе", "харе", "кришна", "кришна", ":-O"];
alert(unique(str));*/

/*let map = new Map([
    ["banan", 100],
    ["orange", 200],
    ["meat", 4]
]);
map.set("vino", 600);
let obj = Object.fromEntries(map);
console.log(obj);*/

/*function unique(arr) {
    return Array.from(new Set(arr));
}
let str = ["кришна", "кришна", "харе", "харе", "харе", "харе", "кришна", "кришна", ":-O"];
alert(unique(str));*/

/*function aclean(arr) {
    let map = new Map();
    for (let word of arr) {        
        map.set(word.toLowerCase().split('').sort().join(''), word);
    }
    return Array.from(map.values());
}
let arr = ["apn", "teachers", "cheaters", "PAN", "ear", "era", "hectares"];
console.log(aclean(arr));*/

/*let map = new Map();
map.set("name", "John");
let keys = Array.from(map.keys());
keys.push("More");
console.log(keys);*/

/*let john = {'name': 'John', 'age': 30};
let weakMapJohn = new WeakMap();
weakMapJohn.set(john, "hier John");
console.log(weakMapJohn.get(john));
john = null;
console.log(weakMapJohn.get(john)); // null
console.log(john); // null*/

/*let john = {'name': 'John', 'age': 30};
let weakSet = new WeakSet();
weakSet.add(john);
console.log(weakSet.has(john)); // true
john = null;
console.log(weakSet.has(john)); // false
console.log(john); // null*/

/*let A = [1, 2, 3, 4, 5, 6, 7, 8, 9];
console.log(Object.entries(A));*/

/*let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250,
};
for (let price of Object.values(salaries) ) {
    console.log(price);
}*/

/*let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250,
};
console.log(Object.keys(salaries).reduce((s, a, b) => s + 1, 0));
console.log(Object.keys(salaries).length);*/

/*let user = {
    name: "John",
    years: 30
};
let {name, years:age, isAdmin = false} = user;
alert(name);
alert(age);
alert(isAdmin);*/

/*let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250,
};
function topSalary(salaries) {
    let lider = null, maxSalary = 0;
    for (const [name, salary] of Object.entries(salaries)) {
        if (maxSalary < salary) [lider, maxSalary] = [name, salary];
    }
    return lider;
}
console.log(topSalary(salaries));*/

/*let dat = new Date(-100000000);
alert(dat.getFullYear());*/

/*let date = new Date();
date.setFullYear(2012, 1, 20);
date.setHours(15, 12, 0);
alert(date);*/

/*let date2 = new Date(2012, 1, 20, 3, 12, 0);
alert(date2);*/

/*function getWeekDay(date) {
    let weekName = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];
    return weekName[date.getDay()];
}
let date = new Date(2020, 8, 2);
alert(getWeekDay(date));*/

/*function getWeekDay(date) {
    let weekName = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];
    let numberWeekDay = date.getDay();
    if (numberWeekDay === 0) numberWeekDay = 7; 
    return weekName[numberWeekDay];
}
let date = new Date(2020, 8, 2);
alert(getWeekDay(date));*/

/*function getDateAgo(date, days) {
    return new Date(date.setDate(date.getDate() - days)).getDate();
    // или
    date.setDate(date.getDate() - days);
    return date.getDate();
}
let [date, days] = [new Date(2015, 0, 2), 2];
alert(getDateAgo(date, days));*/

/*function getLastDayOfMonth(year, month) {
    return new Date(year, month + 1, 0).getDate();
}
alert(getLastDayOfMonth(2012, 1));*/

/*function getSecondsToday() {
    let currentYear = new Date().getFullYear();
    let currentMonth = new Date().getMonth();
    let currentDate = new Date().getDate();
    let currentDay = new Date(currentYear, currentMonth, currentDate);
    return (new Date() - currentDay) / 1000;
    // или
    let currentDay = new Date().setHours(0, 0, 0, 0);
    return (new Date() - currentDay) / 1000;
    // или
    let d = new Date();
    return d.getHours() * 3600 + d.getMinutes() * 60 + d.getSeconds();
}
alert(getSecondsToday());*/

/*function getSecondsToTomorrow() {
    let curentDate = new Date();
    let tomorrow = new Date();
    tomorrow.setDate(curentDate.getDate() + 1);
    tomorrow.setHours(0, 0, 0, 0);
    // или
    tomorrow = new Date(curentDate.getFullYear(), curentDate.getMonth(), curentDate.getDate() + 1);
    return (tomorrow - curentDate) / 1000;
}
alert(getSecondsToTomorrow());*/

/*function formatDate(date) {
    let diff = new Date() - date; // разница в миллисекундах
    if (diff < 1000) { // меньше 1 секунды
        return 'прямо сейчас';
    }
    let sec = Math.floor(diff / 1000); // преобразовать разницу в секунды
    if (sec < 60) {
        return sec + ' сек. назад';
    }
    let min = Math.floor(diff / 60000); // преобразовать разницу в минуты
    if (min < 60) {
        return min + ' мин. назад';
    }
    // отформатировать дату
    // добавить ведущие нули к единственной цифре дню/месяцу/часам/минутам
    let d = date;
    d = [
        '0' + d.getDate(),
        '0' + (d.getMonth() + 1),
        '' + d.getFullYear(),
        '0' + d.getHours(),
        '0' + d.getMinutes()
    ].map(component => component.slice(-2)); // взять последние 2 цифры из каждой компоненты
    // соединить компоненты в дату
    return d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
}
alert(formatDate(new Date(new Date - 1))); // "прямо сейчас"
alert(formatDate(new Date(new Date - 30 * 1000))); // "30 сек. назад"
alert(formatDate(new Date(new Date - 5 * 60 * 1000))); // "5 мин. назад"
// вчерашняя дата вроде 31.12.2016, 20:00
alert(formatDate(new Date(new Date - 86400 * 1000)));*/

/*function formatDate(date) {
    let dayOfMonth = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let hour = date.getHours();
    let minutes = date.getMinutes();
    let diffMs = new Date() - date;
    let diffSec = Math.round(diffMs / 1000);
    let diffMin = diffSec / 60;
    let diffHour = diffMin / 60;
    // форматирование
    year = year.toString().slice(-2);
    month = month < 10 ? '0' + month : month;
    dayOfMonth = dayOfMonth < 10 ? '0' + dayOfMonth : dayOfMonth;
    hour = hour < 10 ? '0' + hour : hour;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    if (diffSec < 1) {
        return 'прямо сейчас';
    } else if (diffMin < 1) {
        return `${diffSec} сек. назад`
    } else if (diffHour < 1) {
        return `${diffMin} мин. назад`
    } else {
        return `${dayOfMonth}.${month}.${year} ${hour}:${minutes}`
    }
}*/

/*let user = {
    name: "Василий Иванович",
    age: 35
};
let json = JSON.stringify(user);
alert(json);
let new_user = JSON.parse(json);
console.log(new_user);*/

/*function sumTo(n) {
    return (n == 1) ? n : n + sumTo(n-1);
}
alert(sumTo(100));*/

/*function sumTo(n) {
    let result = 0;
    for (n; n > 0; n--){
        result += n;
    }
    return result;
}
alert(sumTo(100));*/

/*function sumTo(n) {
    return (1 + n) / 2 * n;
}
alert(sumTo(100));*/

/*function factorial(n) {
    return (n == 1) ? n : n * factorial(n - 1);
}
alert( factorial(5) ); // 120*/

/*// Только для небольших чисел
function fib(n) {
    return n <= 1 ? n : fib(n - 1) + fib(n - 2);
}
alert(fib(3)); // 2
alert(fib(7)); // 13*/

/*function sumAll(...args) { // args — имя массива
    let sum = 0;
    for (let arg of args) sum += arg;
    return sum;
}
alert(sumAll(1)); // 1
alert(sumAll(1, 2)); // 3
alert(sumAll(1, 2, 3)); // 6*/

/*function getUserParam(id, name, ...rest) {
    alert(`id и имя - ${id} : ${name}!`);
    alert(`остальные параметры - ${rest}`);
}
let [id, name, age, position, firsDay] = [1, "Denis", 40, "boss", "01.11.1980"];
getUserParam(id, name, age, position, firsDay);*/

/*function getUserParam(...rest) {
    alert(`rest - ${rest}`);
    alert(`arguments - ${arguments[0]} ${arguments[1]} ${arguments[2]} ${arguments[3]} ${arguments[4]} ${arguments[5]} ${arguments[6]}`); 
    // arguments - это объект, поэтому нельзя его отобразить как массив
    // ${arguments[5]} ${arguments[6]} - undefined
}
let [id, name, age, position, firsDay] = [1, "Denis", 40, "boss", "01.11.1980"];
getUserParam(id, name, age, position, firsDay);*/

/*let arr1 = [1, "Denis", 40];
let arr2 = ["boss", "01.11.1980"];
let param1 = "note1";
let param2 = "note2";
let sumArr = [...arr1, param1, ...arr2, param2]; // создается новый массив
alert(sumArr);
arr1[0] = 2; // при изменении части старого массива
alert(arr1);
alert(sumArr); // новый не изменяется*/

/*let name = "John";
function seyHi() {
    alert(`Hi, ${name}`);
}
name = "Pete";
seyHi(); // Hi, Pete*/

/*function sum(a) {
    return function (b) {
        return function (c) {
            return a + b + c;
        }
    }
}
alert(sum(5)(5)(5));*/

/*let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
function inBetween(a, b) {
    return function(x) {
        return x >= a && x <=b;
    }
}
function inArray(arr) {
    return function(x) {
        return arr.includes(x);
    }
}
alert(arr.filter(inBetween(3, 6)));
alert(arr.filter(inArray([1, 2, 40])));*/

/*let users = [
    {name: "Вася", surname: "Пупкин", age: 25, id: 1,},
    {name: "Петя", surname: "Иванов", age: 10, id: 2,},
    {name: "Маша", surname: "Петрова", age: 28, id: 3,},
];
function byField(fild) {
    return function(a, b) {
        return a[fild] > b[fild] ? 1 : -1;
    }
}
users.sort(byField('name'));
users.forEach(user => alert(user.name));
users.sort(byField('age'));
users.forEach(user => alert(user.age));*/

/*function test() {
    let a = 10;
    return function () {
        a++;
        alert(a);
    }
}
let a = 1;
let test2 = test();
test2(); // 11
test2(); // 12
let test3 = test();
test3(); // 11*/

/*//var myGlobalX = 1980;
if (!window.myGlobalX) alert("Нет такой переменной");*/

/*function sayHi() {
    alert("Hi");
    // давайте посчитаем, сколько вызовов мы сделали
    sayHi.counter++;
}
sayHi.counter = 0; // начальное значение
sayHi(); // Hi
sayHi(); // Hi
alert(`Вызвана ${sayHi.counter} раза`); // Вызвана 2 раза*/

/*let sayHi = function func(who) {
    if (who) {
        alert(`Hello, ${who}`);
    } else {
        func("Guest"); // Теперь всё в порядке
    }
};
let welcome = sayHi;
sayHi = null;
welcome(); // Hello, Guest (вложенный вызов работает)*/

/*function makeCounter() {
    let count = 0;
    function counter() {
        return ++count;
    }
    counter.set = (val) => count = val;
    counter.decrease = () => --count;
    return counter;
}
let iCounter = makeCounter();
alert(iCounter()); // 1
alert(iCounter()); // 2
alert(iCounter()); // 3
iCounter.decrease(); // -1
iCounter.decrease(); // -1
alert(iCounter()); // 2
iCounter.set(5); // = 5
alert(iCounter()); // 6
alert(iCounter()); // 7*/

/*let func = new Function('a', 'b', 'return a + b'); // Не имеет доступ к внешнему окружению, только к глобальному
alert(func(5, 6));*/

/*function sayHi(phrase, name) {
    alert(`${phrase} ${name}!`);
}
setTimeout(sayHi, 2000, "Привет", "Андрей");*/

/*function sayHi(phrase, name) {
    console.log(`${phrase} ${name}!`);
}
setInterval(sayHi, 2000, "Привет", "Андрей");*/

/*let timerId = setTimeout(function tick() {
    alert('tick');
    timerId = setTimeout(tick, 2000);
}, 2000);*/

/*function printNumbers(from, to) {
    let currentNumber = from;
    function go() {
        alert(currentNumber);
        if (currentNumber < to) {
            currentNumber++;
            setTimeout(go, 2000);
        }
    }
    setTimeout(go, 2000);
}
printNumbers(3, 8);*/
// или
/*function printNumbers(from, to) {
    let currentNumber = from;
    setTimeout(function go() {
        alert(currentNumber);
        if (currentNumber < to) {
            currentNumber++;
            setTimeout(go, 2000);
        }
    }, 2000);
}
printNumbers(3, 8);*/

/*function printNumbers(from, to) {
    let currentNumber = from;
    function funcAlert() {
        if (currentNumber >= to) {
            clearInterval(timerId);
        }
        alert(currentNumber);
        currentNumber++;
    }
    let timerId = setInterval(funcAlert, 2000);
}
printNumbers(6, 8);*/
// или
/*function printNumbers(from, to) {
    let currentNumber = from;
    let timerId = setInterval(() => {
        if (currentNumber >= to) {
            clearInterval(timerId);
        }
        alert(currentNumber);
        currentNumber++;
    }, 2000);
}
printNumbers(6, 8);*/

/*function work(a, b) {
    return a + b;
}
function spy(func) {
    function wrapper(...arg){
        wrapper.calls.push(arg);
        let result = func.apply(this, arguments);
        return result;
    }
    wrapper.calls = [];
    return wrapper;
}
work = spy(work);
alert(work(1, 2)); // 3
alert(work(4, 5)); // 9
for (let args of work.calls) {
    alert('call:' + args.join()); // "call:1,2", "call:4,5"
}*/

/*function f(x) {
    alert(x);
}
function delay(func, time) {
    return function wrapper(x) {
        setTimeout(() => func.call(this, x), time);
    }
}
console.log(delay(f, 1000));
let f1000 = delay(f, 1000);
f1000("test");*/

/*function debounce(func, time) {
    wrapper.start = null;
    function wrapper(str) {
        if (wrapper.start == null || Date.now() - wrapper.start > time) {
            func.call(this, str);
            wrapper.start = Date.now();
        }
    }
    return wrapper;
}
let f = debounce(alert, 1000);
f(1); // выполняется немедленно
f(2); // проигнорирован
setTimeout(() => f(3), 100); // проигнорирован (прошло только 100 мс)
setTimeout(() => f(4), 1100); // выполняется
setTimeout(() => f(5), 1500); // проигнорирован (прошло только 400 мс от последнего вызова)*/
// или 
/*function debounce(f, ms) {
    let isCooldown = false;
    return function () {
        if (isCooldown) return;
        f.apply(this, arguments);
        isCooldown = true;
        setTimeout(() => isCooldown = false, ms);
    };
}*/

/*function f(a) {
    console.log(a)
}
function throttle(f, ms) {
    let isThrottled = false,
        savedArgs,
        savedThis;
    function wrapper() {
        if (isThrottled) { // (2)
            savedArgs = arguments;
            savedThis = this;
            return;
        }
        f.apply(this, arguments); // (1)
        isThrottled = true;
        setTimeout(function () {
            isThrottled = false; // (3)
            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                savedArgs = savedThis = null;
            }
        }, ms);
    }
    return wrapper;
}
// f1000 передаёт вызовы f максимум раз в 1000 мс
let f1000 = throttle(f, 1000);
f1000(1); // показывает 1
f1000(2); // (ограничение, 1000 мс ещё нет)
f1000(3); // (ограничение, 1000 мс ещё нет)*/

/*let user = {
    name: "Peter",
    sayHi: function() {
        alert("Hi " + this.name);
    }
};
let user2 = {
    name: "Ivan",
};
user.sayHi(); // Hi Peter
let peter = user.sayHi.bind(user2);
peter(); // Hi Ivan*/

/*let group = {
    title: "Our Group",
    students: ["John", "Pete", "Alice"],
    showList() {
        this.students.forEach(
            student => alert(this.title + ': ' + student)
        );
    }
};
group.showList();*/

/*let dictionary = Object.create(null, {
    toString: {
        value() {
            return Object.keys(this).join();
        },
    }
});
dictionary.apple = "Apple";
dictionary.__proto__ = "Test";
for (let key in dictionary) {
    alert(key);
}
alert(dictionary);*/

/*let user = {
    name: "Denis",
    surname: "Denisow",
    get fullName() {
        return `${this.surname} ${this.name}`;
    },
    get miniName() {
        return `${this.surname} ${this.name[0]}.`;
    }
}
alert(user.fullName);
alert(user.miniName);*/

