// Формат JSON
let json = JSON.stringify(value, [replacer], [space]); 
// value - значение для кодировки, 
// replacer - массив свойств для кодировки или функция соответствия function(key, value)
// space - дополнительные отступы, используемые для форматирования

let value = JSON.parse(str, [reviver]);
// str - JSON для преобразования в объект
// reviver - необязательная функция, которая будет вызываться для каждой пары (ключ, значение) и может преобразовывать значение

