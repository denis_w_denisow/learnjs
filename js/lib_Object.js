// Object

let user = new Object(); // синтаксис "конструктор объекта"
let user = {};  // синтаксис "литерал объекта"

let obj = {};
obj.name = "Мой объект";

let user = {     // объект
    name: "John",  // под ключом "name" хранится значение "John"
    age: 30        // под ключом "age" хранится значение 30
};

// получаем свойства объекта:
alert(user.name); // John
alert(user.age); // 30

let user = {
    name: "John",
    age: 30,
    "likes birds": true  // имя свойства из нескольких слов должно быть в кавычках
};

let user = {};

// присваивание значения свойству
user["likes birds"] = true;

// получение значения свойства
alert(user["likes birds"]); // true

// удаление свойства
delete user["likes birds"];

function makeUser(name, age) {
    return {
        name, // то же самое, что и name: name
        age   // то же самое, что и age: age
        // ...
    };
}

// Перебор свойств объекта 
for (key in object) {
    // тело цикла выполняется для каждого свойства объекта
}
let user = {
    name: "John",
    age: 30,
    isAdmin: true
};

for (let key in user) {
    // ключи
    alert(key);  // name, age, isAdmin
    // значения ключей
    alert(user[key]); // John, 30, true
}

// Клонирование объектов
Object.assign(dest, [src1], [src2], [srcN])

// Аргументы dest, и src1, ..., srcN (может быть столько, сколько нужно) являются объектами.
// Метод копирует свойства всех объектов src1, ..., srcN в объект dest. То есть, свойства всех перечисленных объектов, начиная со второго, копируются в первый объект. 
// После копирования метод возвращает объект dest.

// Например, объединим несколько объектов в один:

let user = { name: "John" };

let permissions1 = { canView: true };
let permissions2 = { canEdit: true };

// копируем все свойства из permissions1 и permissions2 в user
Object.assign(user, permissions1, permissions2);

user = { name: "John", canView: true, canEdit: true }

// Если принимающий объект (user) уже имеет свойство с таким именем, оно будет перезаписано:

// Флаги и дескрипторы
// Помимо значения value, свойства объекта имеют три специальных атрибута (так называемые «флаги»).

writable // – если true, свойство можно изменить, иначе оно только для чтения.
enumerable // – если true, свойство перечисляется в циклах, в противном случае циклы его игнорируют.
configurable // – если true, свойство можно удалить, а эти атрибуты можно изменять, иначе этого делать нельзя.

// обычно они скрыты. Когда мы создаём свойство «обычным способом», все они имеют значение true. 
// Но мы можем изменить их в любое время.

// Метод 
Object.getOwnPropertyDescriptor
// позволяет получить полную информацию о свойстве.

//Его синтаксис:

let descriptor = Object.getOwnPropertyDescriptor(obj, propertyName);

// obj - Объект, из которого мы получаем информацию.
// propertyName - Имя свойства. 
let user = {
    name: "John"
};

let descriptor = Object.getOwnPropertyDescriptor(user, 'name');

alert(JSON.stringify(descriptor, null, 2));
/* дескриптор свойства:
{
"value": "John",
"writable": true,
"enumerable": true,
"configurable": true
}
*/

// Чтобы изменить флаги, мы можем использовать метод Object.defineProperty.
// Его синтаксис:

Object.defineProperty(obj, propertyName, descriptor)

// obj, propertyName - Объект и его свойство, для которого нужно применить дескриптор.
// descriptor - Применяемый дескриптор. 
let user = {};

Object.defineProperty(user, "name", {
    value: "John"
});

let descriptor = Object.getOwnPropertyDescriptor(user, 'name');

alert(JSON.stringify(descriptor, null, 2));
/*
{
  "value": "John",
  "writable": false,
  "enumerable": false,
  "configurable": false
}
 */

// Сделаем свойство user.name доступным только для чтения. Для этого изменим флаг writable:

let user = {
    name: "John"
};

Object.defineProperty(user, "name", {
    writable: false
});

user.name = "Pete"; // Ошибка: Невозможно изменить доступное только для чтения свойство 'name'

// Теперь никто не сможет изменить имя пользователя, если только не обновит соответствующий флаг новым вызовом defineProperty.

// Существует метод Object.defineProperties(obj, descriptors), который позволяет определять множество свойств сразу.

Object.defineProperties(obj, {
    prop1: descriptor1,
    prop2: descriptor2
    // ...
});

Object.defineProperties(user, {
    name: { value: "John", writable: false },
    surname: { value: "Smith", writable: false },
    // ...
});

// Чтобы получить все дескрипторы свойств сразу, можно воспользоваться методом Object.getOwnPropertyDescriptors(obj).
// Вместе с Object.defineProperties этот метод можно использовать для клонирования объекта вместе с его флагами:

let clone = Object.defineProperties({}, Object.getOwnPropertyDescriptors(obj));

// Обычно при клонировании объекта мы используем присваивание, чтобы скопировать его свойства:

for (let key in user) {
    clone[key] = user[key]
}

// …Но это не копирует флаги. Так что если нам нужен клон «получше», предпочтительнее использовать Object.defineProperties.
// Другое отличие в том, что for..in игнорирует символьные свойства, а Object.getOwnPropertyDescriptors возвращает дескрипторы всех свойств, включая свойства-символы.

// Дескрипторы свойств работают на уровне конкретных свойств.
// Но ещё есть методы, которые ограничивают доступ ко всему объекту:

Object.preventExtensions(obj)
// Запрещает добавлять новые свойства в объект.
Object.seal(obj)
// Запрещает добавлять/удалять свойства. Устанавливает configurable: false для всех существующих свойств.
Object.freeze(obj)
// Запрещает добавлять/удалять/изменять свойства. Устанавливает configurable: false, writable: false для всех существующих свойств.

// А также есть методы для их проверки:

Object.isExtensible(obj)
// Возвращает false, если добавление свойств запрещено, иначе true.
Object.isSealed(obj)
// Возвращает true, если добавление/удаление свойств запрещено и для всех существующих свойств установлено configurable: false.
Object.isFrozen(obj)
// Возвращает true, если добавление/удаление/изменение свойств запрещено, и для всех текущих свойств установлено configurable: false, writable: false. 

// Свойства - геттеры и сеттеры
let obj = {
    get propName() {
        // геттер, срабатывает при чтении obj.propName
    },

    set propName(value) {
        // сеттер, срабатывает при записи obj.propName = value
    }
};

let user = {
    name: "John",
    surname: "Smith",

    get fullName() {
        return `${this.name} ${this.surname}`;
    },

    set fullName(value) {
        [this.name, this.surname] = value.split(" ");
    }
};

// set fullName запустится с данным значением
user.fullName = "Alice Cooper";

alert(user.name); // Alice
alert(user.surname); // Cooper