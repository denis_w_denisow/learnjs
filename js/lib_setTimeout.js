// setTimeout() setInterval()

// setTimeout()
let timerId = setTimeout(func|Code, [delay], [arg1], [arg2], [argN]);
clearTimeout(timerId); // отменяет выполнение таймера по его идентификатору

// func|Code - функция или строка кода
// delay - задержка в миллисекундах (1000 мс = 1 с), по умолчанию 0
// arg1 - argN - аргументы передаваймые в функцию (не поддерживается в IE9)

function sayHi(phrase, name) {
    alert(`${phrase} ${name}!`);
}
setTimeout(sayHi, 2000, "Привет", "Андрей");

// setInterval()
let timerId = setInterval(func|Code, [delay], [arg1], [arg2], [argN]);
// func|Code - функция или строка кода
// delay - период повторений в миллисекундах (1000 мс = 1 с)
// arg1 - argN - аргументы передаваймые в функцию (не поддерживается в IE9)
clearTimeout(timerId); // отменяет выполнение таймера по его идентификатору

function sayHi(phrase, name) {
    alert(`${phrase} ${name}!`);
}
setInterval(sayHi, 2000, "Привет", "Андрей");

// рекурсивный setTimeout()
let timerId = setTimeout(function tick() {
    alert('tick');
    timerId = setTimeout(tick, 2000);
}, 2000);
// В отличае от setInterval() рекурсивный setTimeout() позволяет задавать новый интервал до следующего вызова в зависимости от выполнения функции
// В примере следующий alert() вызывается ровно через 2 с после завершения текущего