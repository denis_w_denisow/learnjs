// ДАТА

let nov = new Date();

new Date(year, month, date, hours, minutes, seconds, ms); // Обязательные только первые два параметра

nov.getFullYear(); // 4 цифры года
nov.getMonth(); // от 0 до 11 месяц
nov.getDate(); // день от 1 до 31
nov.getDay(); // дни недели (0 воскресенье - 6 суббота) 
nov.getHours(); // часы
nov.getMinutes(); // минуты
nov.getSeconds(); // секунды
nov.getMilliseconds(); // миллисекунды
nov.getTime(); // таймштамп - количество миллисекунд с 1 января 1970 года
nov.getTimezoneOffset(); // разница в минутах между местным временем и UTC

nov.setFullYear(year, [month], [date]); 
nov.setMonth(month, [date]);
nov.setDate(date);
nov.setHours(hour, [min], [sec], [ms]);
nov.setMinutes(min, [sec], [ms]);
nov.setSeconds(sec, [ms]);
nov.setMilliseconds(milliseconds);

// Все методы доступны с UTC, например getDate() => getUTCDate() или setDate() => setUTCDate()

let timeshtamp = Date.now(); // быстрая метка текущего времени, аналогично new Date().getTime()

Date.parse(str); // Преобразует из строки в таймштамп 'YYYY-MM-DDTHH:mm:ss.sssZ'